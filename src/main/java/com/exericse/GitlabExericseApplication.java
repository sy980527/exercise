package com.exericse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabExericseApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabExericseApplication.class, args);
    }

}
